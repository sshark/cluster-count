# Cluster Count
A React application written in Scala.js for counting the number of disconnected clusters on the screen. Clusters are created by clicking on the empty slots on the left side of the screen.
 
# Setup
Use `dev` during development to auto reload the changes made to the source files. 

Use `build` to build the final artifacts and copied to the `build/` directory.   

Commands like `compile` and `test` to compile sources and run test cases will work as usual.