if (process.env.NODE_ENV === "production") {
    const opt = require("./clusters-count-opt.js");
    opt.main();
    module.exports = opt;
} else {
    var exports = window;
    exports.require = require("./clusters-count-fastopt-entrypoint.js").require;
    window.global = window;

    const fastOpt = require("./clusters-count-fastopt.js");
    fastOpt.main()
    module.exports = fastOpt;

    if (module.hot) {
        module.hot.accept();
    }
}
