package org.teckhooi.clusterfinder

import slinky.core._
import slinky.core.annotations.react
import slinky.core.facade.ReactElement
import slinky.web.html._

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import org.teckhooi.clusterfinder.components.{Cells, Status}

@JSImport("resources/App.css", JSImport.Default)
@js.native
object AppCSS extends js.Object

@react class App extends Component {
  type Props = Unit

  case class State(xs: List[Boolean])

  override def initialState: State = State(List.fill(25)(false))

  private val css = AppCSS

  def toggleCell(ndx: Int, cellState: Boolean): Unit =
    setState(State(state.xs.updated(ndx, !state.xs(ndx))))

  def resetCells(): Unit =
    setState(initialState)

  def render(): ReactElement = div(className := "App row")(
    div(className := "col-3"),
    Cells(state.xs, toggleCell _),
    Status(state.xs, resetCells _),
    div(className := "col-3"))

}
