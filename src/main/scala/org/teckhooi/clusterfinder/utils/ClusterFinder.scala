package org.teckhooi.clusterfinder.utils

object ClusterFinder {
  def findClusters(layout: List[List[Boolean]], x: Int = 0, y: Int = 0, acc: Set[Set[(Int, Int)]] = Set.empty): Set[Set[(Int, Int)]] = {
    layout match {
      case Nil => acc
      case z :: zs => findClusters(zs, 0, y + 1, singleLine(z, 0, y, acc))
    }
  }

  private def singleLine(line: List[Boolean], x: Int, y: Int, acc: Set[Set[(Int, Int)]]): Set[Set[(Int, Int)]] = {
    line match {
      case Nil => acc
      case z :: zs => singleLine(zs, x + 1, y, single(z, x, y, acc))
    }
  }

  private def single(c: Boolean, x: Int, y: Int, acc: Set[Set[(Int, Int)]]): Set[Set[(Int, Int)]] = {
    if (c) {
      val checks: Set[(Int, Int)] = Set((x - 1, y), (x, y - 1))
      val (hits, misses) = acc.partition(xs => xs.intersect(checks).nonEmpty)
      misses + (hits.foldLeft(Set.empty[(Int, Int)])(_ ++ _) + ((x, y)))
    } else acc
  }
}
