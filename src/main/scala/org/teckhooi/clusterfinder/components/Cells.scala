package org.teckhooi.clusterfinder.components

import org.scalajs.dom.Event
import org.scalajs.dom.html.Button
import slinky.core.annotations.react
import slinky.core.facade.ReactElement
import slinky.core.{StatelessComponent, SyntheticEvent}
import slinky.web.html._

@react class Cells extends StatelessComponent {
  case class Props(xs: List[Boolean], toggleCell: (Int, Boolean) => Unit)

  override def render(): ReactElement =
    div(className := "App-grid col-sm-4")(
      (0 to 4).map(i => div(className := "App-grid-row")(buildRow(i))))

  private def buildRow(start: Int) =
    (0 to 4).map(
      j =>
        div(className := "App-grid-cell App-grid-20")(
          button(className := s"App-square App-square-100 ${if (props.xs(start * 5 + j)) {
            "filled"
          } else ""}", id := (start * 5 + j + 1).toString, onClick := (handleClick(_)))))

  def handleClick(e: SyntheticEvent[Button, Event]): Unit = {
    val ndx = e.target.id.toInt - 1
    props.toggleCell(ndx, !props.xs(ndx))
  }
}
