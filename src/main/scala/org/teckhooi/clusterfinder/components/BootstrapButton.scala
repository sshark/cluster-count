package org.teckhooi.clusterfinder.components

import org.scalajs.dom.Event
import org.scalajs.dom.html.Button
import slinky.core.{ExternalComponent, SyntheticEvent}
import slinky.core.annotations.react

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@JSImport("react-bootstrap/Button", JSImport.Default)
@js.native
object BootstrapButtonDOM extends js.Object

@react object BootstrapButton extends ExternalComponent {
  case class Props(
      id: Option[String] = None,
      onClick: Option[() => Unit] = None,
      variant: String = "primary",
      size: String = "",
  )

  override val component = BootstrapButtonDOM
}
