package org.teckhooi.clusterfinder.components

import slinky.core.ExternalComponent
import slinky.core.annotations.react

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@JSImport("react-animated-number", JSImport.Default)
@js.native
object AnimatedNumberDOM extends js.Object

@react object AnimatedNumber extends ExternalComponent {
  case class Props(
    style: js.Object,
    frameStyle: Double => js.Object,
    stepPrecision: Int,
    value: Int,
    formatValue: Int => String)

  override val component = AnimatedNumberDOM
}
