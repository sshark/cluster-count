package org.teckhooi.clusterfinder.components

import org.scalajs.dom.Event
import org.scalajs.dom.html.Button
import scala.scalajs.js.Dynamic.literal

import org.teckhooi.clusterfinder.utils.ClusterFinder
import slinky.core.annotations.react
import slinky.core.facade.ReactElement
import slinky.core.{StatelessComponent, SyntheticEvent}
import slinky.web.SyntheticMouseEvent
import slinky.web.html._

@react class Status extends StatelessComponent {

  case class Props(xs: List[Boolean], resetCells: () => Unit)

  override def render(): ReactElement = div(className := "App-control col-sm-2")(
    div(
      div(
        div(
          className := "introduction",
          h6(className := "text-left")(strong(u("Description"))),
          p(className := "text-justify")(
            """
              |Click to select a box. Any selected box or group of contagious selected boxes is counted as 1 cluster.
              |Only boxes lie NOT in a diagonal direction are considered contagious. The total number of clusters
              |is displayed in "Total Clusters"""".stripMargin)
        )),
      div(style := literal(marginTop = "64px"))(h5("Total Clusters")),
      div(h1(AnimatedNumber(
        Map("transition" -> "3s ease-out", "transitionProperty" -> "background-color, color"),
        (n: Double) => if (n == 100D) Map.empty[String, String] else Map("background" -> "#ffeb3b"),
        0,
        ClusterFinder.findClusters(props.xs.grouped(5).toList).size,
        (x: Int) => x.toString
      ))),
      div(className := "row")(BootstrapButton(onClick := (() => handleClick()))("Clear"))
    )
  )

  def handleClick(): Unit = props.resetCells()
}
