package org.teckhooi

import org.scalatest.FunSuite
import org.scalatest.Matchers._
import org.teckhooi.clusterfinder.utils.ClusterFinder.findClusters

class ClusterFinderTest extends FunSuite {
  test("3 clusters orientation") {
    val clusters: List[List[Boolean]] = List(
      "     # ",
      "     ##",
      "      #",
      "#   #  ").map(_.toList.map(c => if (c == ' ') false else true))

    findClusters(clusters).size should be(3)

  }

  test("No cluster orientation") {
    val singleBlob: List[List[Boolean]] = List(
      "     ",
      "     ",
      "     ",
      "     ").map(_.toList.map(c => if (c == ' ') false else true))

    findClusters(singleBlob) shouldBe empty
  }

  test("Single blob orientation") {
    val singleBlob: List[List[Boolean]] = List(
      "#####",
      "#####",
      "#####",
      "#####").map(_.toList.map(c => if (c == ' ') false else true))

    findClusters(singleBlob).size should be(1)
  }

  test("Non-uniform orientation") {
    val broken: List[List[Boolean]] = List(
      "     # ",
      "     #",
      "      #",
      "#   #  ").map(_.toList.map(c => if (c == ' ') false else true))

    findClusters(broken).size should be(4)
  }

  test("Special case orientation") {
    val broken: List[List[Boolean]] = List(
      " # # ",
      "# #  ",
      "   # ",
      "#### ",
      "#  # ").map(_.toList.map(c => if (c == ' ') false else true))

    println(findClusters(broken))
    findClusters(broken).size should be(5)
  }
}
